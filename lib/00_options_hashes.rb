def transmogrify(string, opts={})
  defaults = {:times => 1,
        :upcase => false,
        :reverse => false}

  merged = defaults.merge(opts)

  altered = ""

  actions = merged.select {|k, v| v == true}
  altered = string * merged[:times]

  actions.each {|action, val| altered = altered.send(action) unless action == :times}

  altered

end
